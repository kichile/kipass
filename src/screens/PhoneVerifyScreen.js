import React, { Component } from 'react';
import {StatusBar, StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import PhoneInput from 'react-native-phone-input';
import Spacer from '../components/Spacer';
import Logo from '../components/Logo';

class PhoneVerifyScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;
        this.onPressFlag = this.onPressFlag.bind(this);
        this.selectCountry = this.selectCountry.bind(this);
        this.validateNumber = this.validateNumber.bind(this);
        this.sendNumber = this.sendNumber.bind(this);
        this.state = {
            cca2: 'CL',
            callingCode: '56',
            cellphoneNumber: '+569',
            validNumber: true
        };
    }

    onPressFlag() {
        this.countryPicker.openModal();
    }

    selectCountry(country) {
        this.phone.selectCountry(country.cca2.toLowerCase());
        this.setState({
            cca2: country.cca2,
            callingCode: country.callingCode,
            cellphoneNumber: '+'+country.callingCode
        });
    }

    onChangeCellPhoneNumber(number){
        this.setState({
            cellphoneNumber: number
        })
    }

    validateNumber(){

        if(this.phone.isValidNumber()){
            this.setState({
                validNumber: true
            });
            return true;
        }else{
            this.setState({
                validNumber: false
            });
            return false;
        }
        return false;
    }

    sendNumber(){
        const {navigate} = this.props.navigation;
        if(this.validateNumber()){
            // hacer llamada a api enviando el número
            navigate('CodeVerify', {
                cellphoneNumber: this.state.cellphoneNumber,
            });
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor='#006978' barStyle='light-content' />
                <Logo />
                <Text style={styles.initialText}>
                    Verificar celular
                </Text>
                <PhoneInput
                    initialCountry='cl'
                    style={styles.inputBox}
                    ref={(ref) => {
                        this.phone = ref;
                    }}
                    textProps={{placeholder: 'Telephone number'}}
                    value={this.state.cellphoneNumber}
                    onChangePhoneNumber={value => this.onChangeCellPhoneNumber(value)}
                    onPressFlag={this.onPressFlag}
                    autoFormat={true}
                />
                {
                    !this.state.validNumber && (
                        <Text style={styles.notValidNumber}>
                            Número no válido!
                        </Text>
                    )
                }
                <CountryPicker
                    ref={(ref) => {
                        this.countryPicker = ref;
                    }}
                    onChange={value => this.selectCountry(value)}
                    translation="spa"
                    filterable={true}
                    hideAlphabetFilter={true}
                    showCallingCode={true}
                    filterPlaceholder="Filtro"
                    cca2={this.state.cca2}
                    transparent={true}
                    styles={{width:40}}
                >
                    <View />
                </CountryPicker>

                <TouchableOpacity style={styles.button} onPress={this.sendNumber}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                <Spacer space={90} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 250,
        backgroundColor: '#0097a7'
    },
    inputBox:{
        width: 300,
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button:{
        width: 300,
        backgroundColor: '#006978',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    notValidNumber:{
        fontSize: 16,
        fontWeight: '500',
        color: '#56c8d8',
        marginLeft: 35,
        alignSelf: 'flex-start'
    },
    initialText:{
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff'
    }
});
// #92b43c
export default PhoneVerifyScreen;
