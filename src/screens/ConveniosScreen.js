import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

const ConveniosScreen = () => {
    return(
        <View style={styles.container}>
            <SafeAreaView forceInset={{ top: 'always' }}>
                <Text>
                    Convenios...
                </Text>
            </SafeAreaView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
});

export default ConveniosScreen;