import React, { Component } from 'react';
import {StatusBar, StyleSheet, View, TouchableOpacity, Text, TextInput } from 'react-native';
import { validate as validateRut, clean, format } from 'rut.js';
import Spacer from '../components/Spacer';
import Logo from '../components/Logo';

class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.validateRegister = this.validateRegister.bind(this);
        this.setNombre = this.setNombre.bind(this);
        this.setApellido = this.setApellido.bind(this);
        this.setRut = this.setRut.bind(this);
        this.setEmail = this.setEmail.bind(this);
        this.sendRegister = this.sendRegister.bind(this);
        this.setRutOnblur = this.setRutOnblur.bind(this);
        this.setRutOnFocus = this.setRutOnFocus.bind(this);
        this.state = {
            nombre: '',
            apellido: '',
            rut: '',
            email: ''
        };
        // console.disableYellowBox = true;
    }

    setNombre(nombre){
        this.setState({
            nombre: nombre
        });
    }
    setApellido(apellido){
        this.setState({
            apellido: apellido
        });
    }

    setRut(rut){
        // let rutClean = clean(rut);
        // let value = event.nativeEvent.text;
        // let rutClean = clean(value);
        // this.setState({
        //     rut: format(event.nativeEvent.text)
        // });
        this.setState({
            rut: rut
        });
    }

    setRutOnblur(){
        const rut = this.state.rut;
        this.setState({
            rut: format(rut)
        });
    }

    setRutOnFocus(event){
        const rut = this.state.rut;
        this.setState({
            rut: clean(rut)
        });
    }

    setEmail(email){
        this.setState({
            email: email
        });
    }

    validateRegister(){
        // if(!isNaN(this.state.code)){
        //     let numberToString = this.state.code !== null ? this.state.code.toString() : 0;
        //     if(numberToString.length === 6){
        //         navigate('mainFlow');
        //     }
        // }else{
        //
        // }
        return true;
    }

    sendRegister(){
        const {navigate} = this.props.navigation;
        if(this.validateRegister()){
            //hacer llamada a api para registro
            navigate('mainFlow');
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor='#006978' barStyle='light-content' />
                <Logo />

                <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Nombre'
                    placeholderTextColor='#ffffff'
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText={this.setNombre}
                />

                <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Apellido'
                    placeholderTextColor='#ffffff'
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText={this.setApellido}
                />

                <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Rut (sin puntos ni guión)'
                    placeholderTextColor='#ffffff'
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={this.state.rut}
                    onChangeText={this.setRut}
                    onBlur={this.setRutOnblur}
                    onFocus={this.setRutOnFocus}
                />

                <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Email'
                    placeholderTextColor='#ffffff'
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText={this.setEmail}
                    keyboardType={"email-address"}
                />

                <TouchableOpacity style={styles.button} onPress={this.sendRegister}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                <Spacer space={40} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 250,
        backgroundColor: '#0097a7'
    },
    inputBox:{
        width: 300,
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button:{
        width: 300,
        backgroundColor: '#006978',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    notValidCode:{
        fontSize: 16,
        fontWeight: '500',
        color: '#56c8d8',
        marginLeft: 35,
        alignSelf: 'flex-start'
    },
    initialText:{
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        margin: 10
    }
});

export default RegisterScreen;
