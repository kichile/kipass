import React, { Component } from 'react';
import {StatusBar, StyleSheet, View, TouchableOpacity, Text, TextInput } from 'react-native';
import Spacer from '../components/Spacer';
import Logo from '../components/Logo';

class CodeVerifyScreen extends Component {
    constructor(props) {
        super(props);
        this.validateCode = this.validateCode.bind(this);
        this.setCode = this.setCode.bind(this);
        this.sendCode = this.sendCode.bind(this);
        this.state = {
            timer: 119,
            minutes: '02',
            seconds: '00',
            code: null
        };
        // console.disableYellowBox = true;
    }

    componentDidMount(){
        this.interval = setInterval(
            () => this.setState((prevState)=> ({
                    timer: prevState.timer - 1,
                    minutes: '0'+Math.trunc(this.state.timer / 60),
                    seconds: this.state.timer % 60 < 10 ? '0'+this.state.timer % 60 : this.state.timer % 60
                })
            ),
            1000
        );
    }

    componentDidUpdate(){
        if(this.state.timer === -1){
            clearInterval(this.interval);
        }
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }

    setCode(code){
        this.setState({
            code: code
        });
    }

    validateCode(){

        if(!isNaN(this.state.code)){
            let numberToString = this.state.code !== null ? this.state.code.toString() : 0;
            if(numberToString.length === 6){
               return true;
            }
        }else{

        }

        return false;
    }

    sendCode(){
        const {navigate} = this.props.navigation;
        if(this.validateCode()){
            // llamar a api con code antes de pasar a siguiente página
            navigate('Register');
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {
        const { navigation } = this.props;
        const cellphoneNumber = navigation.getParam('cellphoneNumber', 'NO-CELLPHONE');
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor='#006978' barStyle='light-content' />
                <Logo />
                <Text style={styles.initialText}>
                    Verificar {cellphoneNumber}
                </Text>
                <Text style={styles.initialText}>
                    Digite el código que le llegará vía SMS
                </Text>

                <Text style={styles.initialText}>
                    {this.state.minutes}:{this.state.seconds}
                </Text>

                <TextInput
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder='Código'
                    placeholderTextColor='#ffffff'
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={this.state.code}
                    onChangeText={this.setCode}
                    keyboardType={"phone-pad"}
                >
                </TextInput>

                <TouchableOpacity style={styles.button} onPress={this.sendCode}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                <Spacer space={90} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 250,
        backgroundColor: '#0097a7'
    },
    inputBox:{
        width: 300,
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button:{
        width: 300,
        backgroundColor: '#006978',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    notValidCode:{
        fontSize: 16,
        fontWeight: '500',
        color: '#56c8d8',
        marginLeft: 35,
        alignSelf: 'flex-start'
    },
    initialText:{
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        margin: 10
    }
});

export default CodeVerifyScreen;
