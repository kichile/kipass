import React, {Component} from 'react';
import { View, StyleSheet } from "react-native";

class Spacer extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <View style={{margin: this.props.space}} >{this.props.children}</View>
        )
    }
}

export default Spacer;
