import React from 'react';
import { View, StyleSheet, Text, Image } from "react-native";
import LOGO from '../../assets/logo.png';

const Logo = () => {
    const shadowStyle = {
        shadowOpacity:0.5,
        shadowRadius:15
    };

    return(
        <View style={styles.container}>
            <View style={[styles.image, shadowStyle]}>
                <Image source={ LOGO }/>
            </View>
            <Text style={styles.logoText}>Bienvenido a KiPass</Text>
        </View>

    )
};

const styles = StyleSheet.create({
    container:{
        // borderColor: 'red',
        // borderWidth: 2,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoText:{
        marginVertical: 15,
        fontSize: 18,
        color: 'rgba(255,255,255,0.7)'
    },
    image:{
        width: 90,
        height:90
    }
});


export default Logo;
