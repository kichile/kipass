import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import HomeScreen from "./src/screens/HomeScreen";
import PhoneVerifyScreen from "./src/screens/PhoneVerifyScreen";
import RegisterScreen from "./src/screens/RegisterScreen";
import CodeVerifyScreen from "./src/screens/CodeVerifyScreen";
import AccountScreen from "./src/screens/AccountScreen";
import ConveniosScreen from "./src/screens/ConveniosScreen";
import { Provider as AuthProvider} from "./src/context/AuthContext";
import Icon from 'react-native-vector-icons/Ionicons';

const switchNavigator = createSwitchNavigator({
  loginFlow: createStackNavigator({
    Login: PhoneVerifyScreen,
    CodeVerify: CodeVerifyScreen,
    Register: RegisterScreen
  }),
  mainFlow: createMaterialBottomTabNavigator({
    Home: {
      screen: HomeScreen,
      navigationOptions:{
        tabBarLabel: 'ID',
        labeled: true,
        tabBarIcon: ({tintoColor}) => (
            <Icon name="ios-card" color={tintoColor} size={20} />
        )
      }
    },
    Convenios: { screen: ConveniosScreen,
      navigationOptions:{
        tabBarLabel: 'Convenios',
        labeled: true,
        tabBarIcon: ({tintoColor}) => (
            <Icon name="ios-apps" color={tintoColor} size={20} />
        )
      }
    },
    Cuenta: { screen: AccountScreen,
      navigationOptions:{
        tabBarLabel: 'Cuenta',
        labeled: true,
        tabBarIcon: ({tintoColor}) => (
            <Icon name="ios-person" color={tintoColor} size={20} />
        )
      }
    }
  },{
    initialRouteName: 'Home',
    order: ['Home', 'Convenios', 'Cuenta'],
    shifting: true,
    activeColor: '#f0edf6',
    inactiveColor: '#3e2465',
    barStyle: {
      backgroundColor: '#008ba3'
    }
  })
});

const App = createAppContainer(switchNavigator);
export default () => {
  return(
      <AuthProvider>
        <App/>
      </AuthProvider>
  );
};
